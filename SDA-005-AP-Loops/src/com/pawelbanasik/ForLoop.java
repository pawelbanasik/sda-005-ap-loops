package com.pawelbanasik;

public class ForLoop {

	public static void main(String[] args) {

		int[] numbers = { 3, 5, 6, 78, 8, 3, -23, 5 };

		for (int i = 0; i < numbers.length; i++) {
			System.out.println("numbers[" + i + "]=" + numbers[i]);
		}

		int[] otherNunumbers = new int[5];

		for (int i = 0; i < numbers.length; i++) {
			System.out.println("numbers[" + i + "]=" + numbers[i]);
		}
	}
}
