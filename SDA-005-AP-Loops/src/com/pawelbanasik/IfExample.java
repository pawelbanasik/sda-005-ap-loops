package com.pawelbanasik;

public class IfExample {

	public static void main(String[] args) {
		int age = 16;
		if (age < 18) {
			System.out.println("No alcohol for you");
		}
		if (age < 18)
			System.out.println("No alcohol for you2");
		System.out.println("And no cigarettes too");

		System.out.println("----------");
		if (age < 18) {
			System.out.println("No alcohol for you2");
			System.out.println("And no cigarettes too");
		}

		System.out.println("----------");
	}
}
